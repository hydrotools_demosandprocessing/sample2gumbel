"""
C.Poulard, E. Leblois, B. Renard and Céline Berni ; INRAE
 v 0.5
 may 2021


about Hydrology :
                demo tool designed for a Master level : first contact with frequential analysis
                let us consider the maximal flood discharge of each year, QY, and let us assume this variable follows a Gumbel distribution (until a certain return period...)
                ... a sample of 10 years will not be sufficient to estimate the "right" Gumbel distribution parameters - check with several tries how the estimated QY(T)
                ... how does the estimation evolves when the sample size increases
                ... confidence interval (soon...)
                ... how can we draw the sample on the same graph as QY(T) : "plotting positions" according to Techegodayen, Hazen and others
                ... corespondance frequency <=> return period

about Python : matplolib as a robust visualization tool
                custom legend
                matplotlib widgets (Checkbutton Slider) : not as good as Tkinter of PyQT, but easier and handy
                add tools to mpl toolbar
"""

from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.lines import Line2D
from matplotlib.widgets import Slider, CheckButtons, Button   # widgets du module matplotlib !
from matplotlib.backend_tools import ToolBase, ToolToggleBase
import numpy as np
from numpy.random import gumbel
from scipy.stats import norm
from Sample2Gumbel_translations import *  #  "import *  "  tolerated because this module is made only for this script
import tkinter as Tk

# Constants (toggle-able variables are initialized at the begining of the main)
LANGUAGE = "francais"

# Classes

class Theoritical_distribution:
    T_Gumbel_plot = [1.1, 2, 5, 10, 20, 50, 100]

    def __init__(self, x0=196, gr=102):
        self.gr = gr
        self.x0 = x0
        self.Q_for_plot = [self.QY_for_T(T) for T in Theoritical_distribution.T_Gumbel_plot]
        self.thresholds_for_color_code()


    def gumbel_sample(self, size=10):
        # using  numpy.random.gumbel to draw a sample
        return gumbel(loc=self.x0, scale=self.gr, size=size)

    def thresholds_for_color_code(self):
        self.QT10 = self.QY_for_T(10)
        self.QT50 = self.QY_for_T(50)
        self.QT100 = self.QY_for_T(100)

        print(f"quantiles : Q(T=10)={self.QT10:.2f}, Q(T=50)={self.QT50:.2f}, Q(T=100)={self.QT100:.2f}")

        self.dico_class_color = dict()
        self.dico_class_color['T < 10'] = "black"
        self.dico_class_color['10 < T < 50'] = "green"
        self.dico_class_color['50 < T < 100'] = "blue"
        self.dico_class_color[' T > 100 '] = "red"

        self.handles = [Line2D([0], [0], marker='o', ls="None", label= key,
                          markerfacecolor=value, markersize=10) for (key, value) in self.dico_class_color.items()]

    def QY_for_T(self, T):
        """ QY(T) for a given Gumbel distribution
           Args:
               T (float) :  return period, in years
               gr, x0 (float) :  scale and position parameters of the Gumbel distribution
           Returns :
               QY : estimated maximal yearly discharge with a return period equal to T
        """
        frequency = 1 - (1 / T)  # frequency for the event 'QY is not exceeded"
        u = -np.log(-np.log(frequency))  # neperian logarithm is np.log(x)
        QY = self.x0 + self.gr * u
        return QY



class Observed_series:
    T_Gumbel_plot = [1.1, 2, 5, 10, 20, 50, 100]

    def __init__(self, real_distrib):
        self.real_distrib = real_distrib
        self.serie_chrono = []
        self.serie_sorted = []
        self.serie_colors = []

        self.gr_sample = None
        self.x0_sample = None

        self.nb_years = None

    def flood_class_color(self, Q):
        """
        color-code for plotting
        Note : this code is based on a "physical" demo where people are invited to draw marbles from a 100-marbles set in a bag,
        with 1 red marble, 1 blue, 8 green and 90 black.
        """
        if Q < self.real_distrib.QT10:
            class_color = self.real_distrib.dico_class_color['T < 10']
        elif Q < self.real_distrib.QT50:
            class_color =  self.real_distrib.dico_class_color['10 < T < 50']
        elif Q < self.real_distrib.QT100:
            class_color =  self.real_distrib.dico_class_color['50 < T < 100']
        else:
            class_color =  self.real_distrib.dico_class_color[' T > 100 ']
        return class_color

    def QY_for_T(self, T):
        """ QY(T) for a given Gumbel distribution
           Args:
               T (float) :  return period, in years
               gr, x0 (float) :  scale and position parameters of the Gumbel distribution
           Returns :
               QY : estimated maximal yearly discharge with a return period equal to T
        """
        frequency = 1 - (1 / T)  # frequency for the event 'QY is not exceeded"
        u = -np.log(-np.log(frequency))  # neperian logarithm is np.log(x)
        QY = self.x0_sample + self.gr_sample * u
        return QY

    def T_forQY(self, Q):
        """ T(QY) for a given Gumbel distribution
           Args:
               QY (float) : maximal yearly discharge
               gr, x0 (float) :  scale and position parameters of the Gumbel distribution
           Returns :
               T(float) : estimated return period (in years)
        """
        u = (Q - self.x0_sample) / self.gr_sample  # neperian logarithm is np.log(x)
        frequency = np.exp(-1 * np.exp(-u))  # frequency for the event 'QY is not exceeded"

        T = 1 / (1 - frequency)

        return T

    def add_obs(self, nb):
        """
        add 10 observations (= 10 max annual discharges) to the existing series in chronological order, self.serie_chrono
        """

        if self.nb_years :
            self.nb_years += nb
        else:
            self.nb_years = nb

        new_obs = self.real_distrib.gumbel_sample(size=nb)
        self.serie_chrono.extend(new_obs)
        self.serie_colors.extend([self.flood_class_color(Q) for Q in new_obs])
        self.serie_sorted.extend(new_obs)
        self.serie_sorted.sort(reverse=True)

        self.estimate_gumbel_distribution()

    def estimate_gumbel_distribution(self):
        """ estimate the 2 parameters of a  Gumbel distribution from the sample, from the method of moments
        Note that :
            - the formulas use "only" the mean and standard deviation of the sample
            - there are other formulas, based on maximum likelihood or qwieighted moments
                (see https://www.researchgate.net/publication/274887821_Estimating_Parameters_of_Gumbel_Distribution_using_the_Methods_of_Moments_probability_weighted_Moments_and_maximum_likelihood)

        Args:
            moyenne (float) :  moyenne de l'échantillon
            ecart_type (float) :  ecart-type de l'échantillon
        Returns :
            gr(float) : paramètre d'échelle, gradex
            x0(float) : paramètre de position

        """

        self.gr_sample = 0.78 * np.std(self.serie_chrono)  # scale parameter
        self.x0_sample = np.mean(self.serie_chrono) - 0.577 * self.gr_sample  #  position parametr

    def confidence_interval(self, un_moins_alpha=0.9):

        alpha = 1 - un_moins_alpha
        freq = 1 - alpha / 2

        u = norm.ppf(freq, loc=0, scale=1)  # , freq = à droite et à gauche

        #  p= 1- (1/T) ; frequency ("non exceedence")
        liste_p = [1 - (1 / T) for T in Observed_series.T_Gumbel_plot]
        liste_q = [self.QY_for_T(T) for T in Observed_series.T_Gumbel_plot]
        liste_wp = [ (-np.sqrt(6)/np.pi) * np.log(-np.log(p) ) + 0.45 for p in liste_p]
        # sx² = écart-type
        sx2surN = np.nanstd(self.serie_sorted) ** 2 / len(self.serie_sorted)
        liste_varqp = [sx2surN * (1 + 1.14 * w + 1.1 * w ** 2) for w in liste_wp]
        haut_intervalle = [q + u * varqp ** (1 / 2) for q, varqp in zip(liste_q, liste_varqp)]
        bas_intervalle = [q - u * varqp ** (1 / 2) for q, varqp in zip(liste_q, liste_varqp)]

        return haut_intervalle, bas_intervalle

# fonction paramétrée calculant la fréquence par une fonction paramétrée par a et b
# puis 2 fonctions calculant la période de retour par la même fonction paramétrée et par une fonction avec valeurs fixes
# EVIDEMMENT IL Y A REDONDANCE pour l'exemple ; la première fonction suffirait !

def freq_emp_parametre(n_annees, a, b):
    # pour la formule de Chegodayev, a = 0.3 et b=0.4

    plotting_positions_freq = [ 1 - (((indice + 1) - a) / ((n_annees + b) )) for indice in range(n_annees)]
    # au lieu d'envoyer une liste brute pour l'affichage,
    # on crée une liste de freq formattées (:.2f = flottants 2 décimales) séparées par des "/"

    return plotting_positions_freq

def T_emp_parametre(n_annees, a , b):
    # T=1/freq de la fonction freq_emp_parametre, cas général de la précédente
    plotting_positions_param  = [((n_annees + b) / ((indice + 1) - a)) for indice in range(n_annees)]

    return plotting_positions_param

def writeQT_in_console():
    n_annees = len(serie_Gumbel.serie_sorted)
    frequencies = [ 1 - (((indice + 1) - a) / ((n_annees + b) )) for indice in range(n_annees)]

    print(f"Estimations de Gumbel / Gumbel estimations, N={n_annees} : gr={serie_Gumbel.gr_sample:.2f}, x0= {serie_Gumbel.x0_sample:.2f}")

    print(" / ".join([f"T({Q:.2f})= {serie_Gumbel.T_forQY(Q):.2f}" for Q in serie_Gumbel.serie_sorted]))
    print(f"Formules empiriques pour représenter l'échantillon  / Plotting positions : a={a:.1f}, b={b:.1f}, N={n_annees} ")
    print("Fréquences empiriques / Plotting positions as frequencies : ", "/ ".join([f"{freq:.2f}" for freq in frequencies]))
    print( "Périodes de retour empiriques / Plotting positions as return periods ",
            " / ".join([f"{1 / freq:.1f}" for freq in frequencies]))

def retracer_parametree(echantillon, a, b):
    # si switch :  au lieu de "updater", on retrace complètement en attendant se pouvoir updater des stems..

    if abscissis_as_return_period:
        x_emp =  T_emp_parametre(len(echantillon), a, b)
    else:
        # freq = [ 1 - (1 / T) for T in T_emp]
        x_emp = freq_emp_parametre(len(echantillon), a, b)

    for item in [serie_Gumbel.stemlines_emp, serie_Gumbel.markerline_emp, serie_Gumbel.baseline_emp]:
        item.remove()

    serie_Gumbel.markerline_emp, serie_Gumbel.stemlines_emp, serie_Gumbel.baseline_emp = ax_T.stem(x_emp, serie_Gumbel.serie_sorted, label=plot_Qempir[LANGUAGE],
                                                                                                   use_line_collection=True)

    plt.setp(serie_Gumbel.stemlines_emp, color='purple', lw=1, alpha=0.5)
    plt.setp(serie_Gumbel.markerline_emp, markersize=2, markeredgecolor='orchid', markeredgewidth=3)
    plt.setp(serie_Gumbel.baseline_emp, visible=False)

    # on retrace
    fig_pp.canvas.draw_idle()

def retracer_Gumbel(serie_Gumbel):

    if abscissis_as_return_period:
        courbe_theor_Gumbel.set_xdata(Theoritical_distribution.T_Gumbel_plot)
    else:
        #freq = [ 1 - (1 / T) for T in T_emp]
        freq = [1 - (1/T) for T in Theoritical_distribution.T_Gumbel_plot]
        courbe_theor_Gumbel.set_xdata(freq)

    # on retrace
    fig_pp.canvas.draw_idle()

def retracer_estim_Gumbel(serie_Gumbel):

    if with_confidence_interval:
        haut_intervalle, bas_intervalle = serie_Gumbel.confidence_interval()
        serie_Gumbel.confid_interval_fill_between.remove()

    T_estim_Gumbel = [serie_Gumbel.T_forQY(Q) for Q in serie_Gumbel.serie_sorted]
    if abscissis_as_return_period:
        serie_Gumbel.courbe_estim_Gumbel.set_data(T_estim_Gumbel, serie_Gumbel.serie_sorted)
        x_max_valeurs = T_estim_Gumbel[:N_serie]
        ax_T.set_xlim(0, max(Theoritical_distribution.T_Gumbel_plot) * 1.1)
        if with_confidence_interval:
            x_IC = Observed_series.T_Gumbel_plot
    else:
        # freq = [ 1 - (1 / T) for T in T_emp]
        freq = [ 1 - (1 / T) for T in T_estim_Gumbel]
        serie_Gumbel.courbe_estim_Gumbel.set_data(freq, serie_Gumbel.serie_sorted)
        x_max_valeurs = freq[:N_serie]
        ax_T.set_xlim(0, 1.1)
        if with_confidence_interval:
            x_IC = [1-1/T for T in Observed_series.T_Gumbel_plot]

    # on retrace
    serie_Gumbel.marker_max_estim_Gumbel.set_data(x_max_valeurs, serie_Gumbel.serie_sorted[:N_serie])
    if with_confidence_interval:
        serie_Gumbel.confid_interval_fill_between = ax_T.fill_between(x_IC, haut_intervalle, bas_intervalle, facecolor = 'yellow', alpha=0.5, label=confid_interv[LANGUAGE] )
    ax_T.set_ylim(0, serie_Gumbel.serie_sorted[0]*1.1)

    fig_pp.canvas.draw_idle()


# https://matplotlib.org/stable/gallery/widgets/slider_demo.html
# https://stackoverflow.com/questions/13656387/can-i-make-matplotlib-sliders-more-discrete
def update_a(val):
    # val = valeur renvoyée par le widget
    global a
    a = val   # comme on a précisé "global a", changer a dans la fonction = changer a du prog principal
    retracer_parametree(serie_Gumbel.serie_sorted, a, b)

def update_b(val):
    # val = valeur renvoyée par le widget
    global b
    b = val # comme on a précisé "global b", changer b dans la fonction = changer b du prog principal
    retracer_parametree(serie_Gumbel.serie_sorted, a, b)

def create_series(event):
    # button "new"
    global serie_Gumbel

    serie_Gumbel = Observed_series(real_distribution)
    serie_Gumbel.add_obs(N_serie)

    print("Première série d'obs / First sample : ", ";".join([f"{Q:.2f}" for Q in serie_Gumbel.serie_chrono]))

    ax_chrono.clear()
    ax_T.clear()
    # LES OBS
    derniere_annee = 1 + len(serie_Gumbel.serie_chrono)
    ax_chrono.vlines(x=range(1, derniere_annee), ymin=0, ymax=serie_Gumbel.serie_chrono)
    ax_chrono.scatter(range(1, derniere_annee), serie_Gumbel.serie_chrono,
                      c=serie_Gumbel.serie_colors, alpha=0.7,
                      label=f"Q(t) ;  {serie_Gumbel.nb_years} " + year[LANGUAGE], marker="*", s=60)

    # the "real" GUMBEL plot does not change with the sample, but changes with chose abscissis (frequency or return period)
    if abscissis_as_return_period:
        x_Gumbel = Theoritical_distribution.T_Gumbel_plot
        x_emp = T_emp_parametre(serie_Gumbel.nb_years, a, b)
        x_estimGumbel = [serie_Gumbel.T_forQY(Q) for Q in serie_Gumbel.serie_sorted]
        if with_confidence_interval:
            x_IC = Observed_series.T_Gumbel_plot
    else:
        x_Gumbel = [1 - (1/T) for T in Theoritical_distribution.T_Gumbel_plot]
        x_emp = [1- (1/T) for T in T_emp_parametre(serie_Gumbel.nb_years, a, b)]
        x_estimGumbel = [1 - (1 / serie_Gumbel.T_forQY(Q)) for Q in serie_Gumbel.serie_sorted]
        if with_confidence_interval:
            x_IC =  [1 - 1/T for T in Observed_series.T_Gumbel_plot]

    if with_confidence_interval:
        haut_intervalle, bas_intervalle = serie_Gumbel.confidence_interval()
        serie_Gumbel.confid_interval_fill_between = ax_T.fill_between(
            x_IC, haut_intervalle, bas_intervalle,
            facecolor='yellow', alpha=0.5, label=confid_interv[LANGUAGE])

    courbe_Gumbel, = ax_T.plot(x_Gumbel, real_distribution.Q_for_plot, alpha=0.7, color='orangered',
                               linewidth=5, solid_capstyle='round',
                               zorder=10, label=plot_QGumbel[LANGUAGE], marker="None", ls='--', markersize=7)
    # plt.stem returns 3 objects : markerline, stemlines, baseline
    serie_Gumbel.markerline_emp, serie_Gumbel.stemlines_emp, serie_Gumbel.baseline_emp = ax_T.stem(x_emp, serie_Gumbel.serie_sorted, label=plot_Qempir[LANGUAGE], use_line_collection=True)

    plt.setp(serie_Gumbel.stemlines_emp, color='purple', lw=1, alpha=0.5)
    plt.setp(serie_Gumbel.markerline_emp, markersize=5, markeredgecolor ='purple', markeredgewidth = 3)
    plt.setp(serie_Gumbel.baseline_emp, visible=False)


    serie_Gumbel.courbe_estim_Gumbel, = ax_T.plot(x_estimGumbel, serie_Gumbel.serie_sorted, color='blue', alpha=0.7,
                                     label=" ajustement Gumbel sur obs",
                                     zorder=10)  #, marker="x", markersize=7)
    serie_Gumbel.marker_max_estim_Gumbel, = ax_T.plot(x_estimGumbel[:N_serie], serie_Gumbel.serie_sorted[:N_serie], color='blue', alpha=0.7,
                                                     ls='None', marker="x", markersize=7, label=f"{N_serie} max Qobs")

    fig_pp.canvas.draw_idle()

    return serie_Gumbel, courbe_Gumbel

def update_series(event):
    # bouton "ajouter des obs" activé
    serie_Gumbel.add_obs(N_serie)
    ax_chrono.clear()
    derniere_annee = 1 + len(serie_Gumbel.serie_chrono)
    ax_chrono.set_title(f"Qmax annuels observés sur {serie_Gumbel.nb_years} années")
    ax_chrono.vlines(x=range(1, derniere_annee), ymin=0, ymax=serie_Gumbel.serie_chrono, colors=serie_Gumbel.serie_colors) #, ls="--')
    ax_chrono.scatter(range(1, derniere_annee), serie_Gumbel.serie_chrono,
                      c=serie_Gumbel.serie_colors, alpha=0.7, marker="*", s=20)
    retracer_estim_Gumbel(serie_Gumbel)
    retracer_parametree(serie_Gumbel.serie_sorted, a, b)
    fig_pp.canvas.draw_idle()

def switch_freq(x_as_return_period):
    global abscissis_as_return_period

    abscissis_as_return_period = x_as_return_period

    retracer_parametree(serie_Gumbel.serie_sorted, a, b)
    retracer_Gumbel(serie_Gumbel.serie_sorted)
    retracer_estim_Gumbel(serie_Gumbel)
    if x_as_return_period:
        ax_T.set_xlim(left=0, right= (serie_Gumbel.nb_years + b)/(1 - a) * 1.1)
        ax_T.set_xlabel(ax_T_xlabel_T[LANGUAGE])

    else:
        # freq = [ 1 - (1 / T) for T in T_emp]
        ax_T.set_xlim(left=0, right=1.1)
        ax_T.set_xlabel(ax_T_xlabel_f[LANGUAGE])


    ax_T.set_ylim(bottom=0, top=max(serie_Gumbel.serie_sorted) * 1.1)

    fig_pp.canvas.draw_idle()

def switch_confid_interval(bool_config_int):
    global with_confidence_interval

    with_confidence_interval = bool_config_int

    if with_confidence_interval:
        haut_intervalle, bas_intervalle = serie_Gumbel.confidence_interval()

        if abscissis_as_return_period:
            x_IC = Observed_series.T_Gumbel_plot
        else:
            x_IC = [1 - 1/T for T in Observed_series.T_Gumbel_plot]

        serie_Gumbel.confid_interval_fill_between = ax_T.fill_between(x_IC, haut_intervalle, bas_intervalle, facecolor='yellow', alpha=0.5)

    elif serie_Gumbel.confid_interval_fill_between:
        serie_Gumbel.confid_interval_fill_between.remove()

    fig_pp.canvas.draw_idle()

# TOOLBAR
# https://stackoverflow.com/questions/52971285/add-toolbar-button-icon-matplotlib?noredirect=1&lq=1

class Gumbel_menu(ToolBase):
    image = "icones/menu.png"
    description = 'Display menu for Gumbel demo'

    def trigger(self, *args, **kwargs):
        message = "add 10 : add 10 new observations \n new = restart with 10 observations"
        message += "\n write figures summing up Q(T) estimated by Gumbel and by plotting positions  \n f <=> T = toggle between frequency and return period"
        message += "\n IC = show/hide confidence intervals"

        Tk.messagebox.showinfo(title="Menu Demo Gumbel", message=message)

class ObsPlusDix(ToolBase):
    image = "icones/plus10obs.png"
    description = 'Add 10 obs'

    def trigger(self, *args, **kwargs):
        update_series('<Button-1>')

class NewDixObs(ToolBase):
    image = "icones/newobs.png"
    description = 'Restart, 10 obs'

    def trigger(self, *args, **kwargs):
        create_series('<Button-1>')

class WriteQT(ToolBase):
    image = "icones/writeQT.png"
    description = 'Write Q(T)'

    def trigger(self, *args, **kwargs):
        writeQT_in_console()


class Toogle_freq_T(ToolToggleBase):
    image = "icones/toggle_fT.png"
    default_keymap = 'x'
    description = 'x as return period ( <=>  frequency)'
    default_toggled = True


    def enable(self, *args):
        switch_freq(True)

    def disable(self, *args):
        switch_freq(False)

class Toogle_conf_int(ToolToggleBase):
    image = "icones/toggle_conf_int.png"
    description = 'show confidence intervals'
    default_toggled = True

    def enable(self, *args):
        switch_confid_interval(True)

    def disable(self, *args):
        switch_confid_interval(False)


# Initializations

a_Tchego, b_Tchego = 0.3 , 0.4  # Chegodayev parameters (as a tuple)
# initialisation with Chegodayev values (can be modified by sliders)
a = a_Tchego
b = b_Tchego

abscissis_as_return_period = True
with_confidence_interval = False

# on va constituer une série d'  "observations"  par ajouts de N_series obs à la fois
real_distribution = Theoritical_distribution()
N_serie = 10

# preparing toolbar modification
plt.rcParams["toolbar"] = "toolmanager"


fig_pp = plt.figure( )
gs = gridspec.GridSpec(6, 2, height_ratios= [7, 1, 7, 1,2, 1])
ax_chrono = plt.subplot(gs[0, :])
ax_legend_colors = plt.subplot(gs[1, :])

ax_T = plt.subplot(gs[2, :])
ax_legend_T = plt.subplot(gs[4, :])
ax_slide_a = plt.subplot(gs[5, 0])
ax_slide_b = plt.subplot(gs[5, 1])


# ax_chb = fig.add_subplot()
plt.subplots_adjust(wspace=1, hspace=0.5,left=0.1,top=0.90,right=0.9,bottom=0.1)
fig_pp.canvas.set_window_title(window_title[LANGUAGE])

fig_pp.suptitle(fig_pp_suptitle[LANGUAGE])
serie_Gumbel, courbe_theor_Gumbel = create_series('button_press_event')
ax_T.set_xlabel(ax_T_xlabel_T[LANGUAGE])


for ax_s in [ax_legend_colors, ax_legend_T, ax_slide_a, ax_slide_b]:
    ax_s.xaxis.set_visible(False)
    ax_s.yaxis.set_visible(False)

for pos in ['right', 'top', 'bottom', 'left']:
    ax_legend_colors.spines[pos].set_visible(False)
    ax_legend_T.spines[pos].set_visible(False)

print(message_first_10_obs[LANGUAGE])

ax_legend_colors.axis('off')
ax_legend_T.axis('off')
ax_legend_colors.legend(handles=real_distribution.handles, ncol=4, fontsize=8, labelspacing=1.5)
legende_T= ax_T.legend()
legende_T.set_visible(False)
handles, labels = ax_T.get_legend_handles_labels()
ax_legend_T.legend(handles, labels , ncol=2,  prop={"size":8})



# Sliders
# a
slider_a = Slider(
    ax_slide_a, "paramètre a ", valmin=0.01, valmax = 0.99, valfmt='%0.2f', valinit=a, color="green")

slider_a.on_changed(update_a)

# b
slider_b = Slider(
    ax_slide_b, "paramètre b ", valmin=0.01, valmax = 0.99, valfmt='%0.2f', valinit=b, color="blue")

slider_b.on_changed(update_b)


# Add custom tools (see matplotlib doc))
tm = fig_pp.canvas.manager.toolmanager
tm.add_tool("Menu", Gumbel_menu)
tm.add_tool("Add 10 obs", ObsPlusDix)
tm.add_tool("New, 10 obs", NewDixObs)
tm.add_tool("Write QT", WriteQT)
tm.add_tool("f <-> T", Toogle_freq_T)
tm.add_tool("conf. int.",    Toogle_conf_int)


# Add an existing tool to new group `obs`.
tbar = fig_pp.canvas.manager.toolbar
tbar.add_tool("Menu", "obs")
tbar.add_tool("Add 10 obs", "obs")
tbar.add_tool("New, 10 obs", "obs")
tbar.add_tool("Write QT", "obs")
tbar.add_tool("f <-> T", "freq")
tbar.add_tool("conf. int.", "freq")


plt.show()

