# HydroTools _ Demo and Processing Tools

Under construction !   Thanks for your feedback

## HydroDemo_Sample2Gumbel

The code was developped for teaching, as an interactive tool to demonstrate the variability of floods, as maximum discharge per year, and the estimation of discharge-period relationship with a Gumbel distribution.
Starting from a theoretical Gumbel distribution defined by 2 parameters, it draws series of  discharges representing as many years of observation (default is 10 values), plots the sample using plotting position formulas and a Gumbel estimate, to compare with the theoretical one.
The user can add values to the sample or start a new series.
The graphs do not mention explicitely "Q" as y_label,  because this too can also be used for rainfall analysis illustration.
Q(T) relationship are diplayed with uncertainty, with either return periods or frequency as absicissis.

It is so far in french, but an option for English captions is on its way (translations implemented as a dictionnaries in a separate python file)

## SEE WIKI PAGES FOR DETAILED EXPLANATIONS

Ref on frequential analysis in hydrology :  
Benjamin Renard, Probabilités et Statistiques appliquées à l'Hydrologie : https://cel.archives-ouvertes.fr/hal-02597049/


![screenshot](/Images/DixAnsAvecCentennaleEtQuinquennale.png)

## HydroDemo_Sample2Gumbel2Damages is on its way
By adding the definition of a discharge-frequency table, for one or more land-uses, Sample2Gumbal2Damages computes the damages per land use and per event, as another chronological time-series, and compares the observed damages per year on average, per land-use and total, to the Annual Average Damage estimated from the damage-frequency curve.

![screenshot](/Images/Damage600yrs.PNG)
