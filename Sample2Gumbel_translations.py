#dictionnary of internationalization of HydroDemo_Sample2Gumbel

message_first_10_obs=dict()
message_first_10_obs["francais"] ="Première série de débits max annuels créée"
message_first_10_obs["english"] ="First series of annual maximum discharge created"

year = dict()
year["francais"] = " années"
year["english"] = " years"

confid_interv = dict()
confid_interv["francais"] = "intervalle de confiance"
confid_interv["english"] = "confidence interval"

# FIGURE

window_title = dict()
window_title["francais"] =("Hydrologie -Démo Gumbel pour les débits max annuels (Inrae)")
window_title["english"] =("Hydrology - Gumbel for annual max discharges (Inrae)")
fig_pp_suptitle =dict()
fig_pp_suptitle["francais"] = f"Démo Gumbel : estimation de la relation Q(T) à partir d'observations"
fig_pp_suptitle["english"] = f"Demo Gumbel : estimating Q(T) from observation "

plot_QGumbel = dict()
plot_QGumbel["francais"] = "'vraie' relation Q(T) du bassin versant"
plot_QGumbel["english"] = "Catchment 'real' Q(T)"

plot_Qempir = dict()
plot_Qempir["francais"] = "T ou f empirique, fonction de a et b"
plot_Qempir["english"]="plotting position as f or T (function of a and b)"

ax_T_xlabel_T=dict()
ax_T_xlabel_T["francais"] ="Période de retour, années"
ax_T_xlabel_T["english"] ="Return period (in years)"

ax_T_xlabel_f=dict()
ax_T_xlabel_f["francais"] ="Fréquence au non-dépassement"
ax_T_xlabel_f["english"] ="Frequency (event not exceeded)"